#!perl -lw
print "P3";
print "1800 3600";
print "255";
$max=0;
$scaling=0.68;# 1.08;
while(<>){
    chomp;
    if ($_ < -8999) {
	print "255 0 0";
	next;
    }
    $max=$_ if($_>$max);
    $_ /=$scaling;
    $_ *= 255;
    $_=int $_;
    $_=255 if($_>255);
    die if $_<0;
    $_=255-$_;
    print "0 $_ 0";
}
print STDERR $max;
