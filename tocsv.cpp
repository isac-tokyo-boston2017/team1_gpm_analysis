#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <cassert>
using std::cerr;
using std::endl;

const int xmax=3600;
const int ymax=1800;
const int image_size=xmax*ymax;
//typedef float image[xmax][ymax];
int main(void){
  // need ulimit -s unlimited or else this will segfault
  float buf[xmax][ymax];
  int code;
  code=fread(&buf[0][0],sizeof(float),image_size,stdin);
  if (code != image_size){
    cerr << "did not come out evenly " << code << endl;
    exit(1);
  } else {
    cerr << "code " << code << endl;
  }

  for(int j=ymax-1;j>=0;--j){
    for(int i=0;i<xmax;++i){
      if(i){
	printf(",");
      }
      printf("%e",buf[i][j]);
    }
    printf("\n");
  }
}
