#! perl -w
use PDL;
use PDL::NetCDF;
$xmax=1800;
$ymax=3600;
print STDERR "reading data\n";
for$fn(</scratch/kenta/isac/nc4/*.nc4>){
    push @allfiles,$fn;
    #print STDERR $fn;
#reverse_dims does not seem to affect the getdim list order below.
    $fs{$fn}=PDL::NetCDF->new($fn,{REVERSE_DIMS=>1});
    $d{$fn} =$fs{$fn}->get('precipitation');
    #print "$fn ",nelem($d{$fn})," ",$d{$fn}->getndims," ",$d{$fn}->getdim(0)," ",$d{$fn}->getdim(1);
    die unless $d{$fn}->getdim(0)==$xmax;
    die unless $d{$fn}->getdim(1)==$ymax;
}
print STDERR "reading data done\n";
@newfiles=sort@allfiles;
for($i=0;$i<@allfiles;++$i){
    die unless $newfiles[$i]eq$allfiles[$i];
}
$outdir='/scratch/kenta/isac/out';
#yes they are in sorted order
for($x=0;$x<$xmax;++$x){
    $sx=sprintf('%04d',$x);
    unless ( -e "$outdir/$sx" ){
	mkdir "$outdir/$sx";
    }
    print STDERR "x=$x\n";
    for($y=0;$y<$ymax;++$y){
	$sy=sprintf('%04d',$y);
	$outstr='';
	for$fn(@allfiles){
	    $value=$d{$fn}->at($x,$y);
	    #print $value;
	    $fv=pack("f",$value);
	    die unless 4==length$fv;
	    $outstr.=$fv;
	}
	#open FO,">$outdir/$sx/$sy" or die;
	#print FO $outstr;
	#close FO;
	print $outstr;
    }
}
print STDERR "done\n";
#print "",PDL::NetCDF::isNetcdf4; yes we are compiled against netcdf4
