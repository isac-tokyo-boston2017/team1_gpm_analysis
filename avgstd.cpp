#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <cassert>
using std::cerr;
using std::endl;

float max=0;
void compute(const float*x, int MAX,bool do_std){
  int n=0;
  float s1=0,s2=0;
  // This should be replaced with a moving average to compensate for climate change
  for(int i=0;i<MAX;++i){
  if(x[i]<-8999){
    continue;
  }
  if(x[i]>max)
    max=x[i];
  n++;
  s1+=x[i];
  s2+=x[i]*x[i];
}
  float mean=-9999;
float stdx=-9999;

if(n>0){
  mean=s1/n;
 }
if (n>1){
  stdx=sqrt((n*s2-s1*s1)/(n*(n-1)));
 }
assert(mean==mean);
assert(stdx==stdx);
if(do_std){
  fwrite(&stdx,sizeof(float),1,stdout);
 }else {
  fwrite(&mean,sizeof(float),1,stdout);
 }
}

const int image_size=3600*1800;
typedef float image[image_size];
int main(int argc, char**argv){
  if (argc<3){
    cerr << "need num_months" << endl;
    exit(1);
  }
  int num_months=atoi(argv[1]);
  bool dostd=atoi(argv[2]);
  cerr << "num_months " << num_months << endl;
  cerr << "dostd " << dostd << endl;
  image* buf=new image[num_months];
  int code;
  int items=0;
  for(int i=0;i<num_months;++i){
    code=fread(buf[i],sizeof(float),image_size,stdin);
    if (code != image_size){
      cerr << "did not come out evenly " << code << endl;
      exit(1);
    }
  }
  float* minibuf= new float[num_months];
  for(int i=0;i<image_size;++i){
    for(int j=0;j<num_months;++j){
      minibuf[j]=buf[j][i];
    }
    compute(minibuf,num_months,dostd);
  }
  cerr << "items " << items << endl;
  cerr << "max " << max << endl;
}
