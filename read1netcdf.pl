#! perl -w
use PDL;
use PDL::NetCDF;
$xmax=1800;
$ymax=3600;

die unless defined($fn=$ARGV[0]);
#reverse_dims does not seem to affect the getdim list order below.
$obj=PDL::NetCDF->new($fn,{REVERSE_DIMS=>1});
$data =$obj->get('precipitation');
#print "$fn ",nelem($d{$fn})," ",$d{$fn}->getndims," ",$d{$fn}->getdim(0)," ",$d{$fn}->getdim(1);
die unless $data->getdim(0)==$xmax;
die unless $data->getdim(1)==$ymax;
$ref=$data->get_dataref;
print ${$ref};
