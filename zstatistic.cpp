#include <cstring>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <cstdio>
#include <cassert>
using std::cerr;
using std::endl;

const int xmax=3600; // rows
const int ymax=1800; // cols
const int image_size=xmax*ymax;
//typedef float image[xmax][ymax];
int main(void){
  // need ulimit -s unlimited or else this will segfault
  float mean[xmax][ymax];
  float stdx[xmax][ymax];
  float buf[xmax][ymax];
  int code;
  FILE*fi=fopen("/scratch/kenta/isac/means","r");
  assert(fi);
  code=fread(mean,sizeof(float),image_size,fi);
  assert(code==image_size);
  fclose(fi);

  fi=fopen("/scratch/kenta/isac/stds","r");
  assert(fi);
  code=fread(stdx,sizeof(float),image_size,fi);
  assert(code==image_size);
  fclose(fi);

  code=fread(buf,sizeof(float),image_size,stdin);
  assert(code==image_size);

  for(int j=ymax-1;j>=0;--j){
    for(int i=0;i<xmax;++i){
      if(i){
	printf(",");
      }
      float z=-9999;
      if(buf[i][j]>-8999 || mean[i][j]>-8999 || stdx[i][j]>-8999){
	z=buf[i][j]-mean[i][j];
	z/=stdx[i][j];
      }
      printf("%e",buf[i][j]);
    }
    printf("\n");
  }
}
